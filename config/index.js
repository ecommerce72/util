const Configstore = require('configstore');
const config = new Configstore(process.env.NODE_ENV || "development");
const _ = require('lodash');
const { logger } = require("@ntiendung/logger")
const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');
const mongoose = require("mongoose");
const fs = require('fs')
const path = require('path')

/**
 * Load config to configStore
 * @param {boolean} loadFromDb whether load more config from database or not
 * @param {string} configName if loadFromDb=true then get config by key "configName"
 * @returns 
 */
const loadConf = async ({ loadFromDb = true, configName } = {}) => {
    //load dotenv
    const PRODUCTION = process.env.NODE_ENV === 'production';
    let extendEnvPath = '';
    const rootDir = fs.realpathSync(process.cwd());
    if (PRODUCTION) extendEnvPath = path.resolve(rootDir, '.env.production')
    else extendEnvPath = path.resolve(rootDir, '.env.local');
    dotenvExpand(dotenv.config({ path: extendEnvPath }));
    dotenvExpand(dotenv.config());


    // load config from local file and from database
    const defaultConfigPath = path.resolve(rootDir, 'app/modules/config/default.js')
    let defaultConfig = {};
    if (fs.existsSync(defaultConfigPath)) defaultConfig = require(defaultConfigPath);

    if (loadFromDb) {
        const cloudConfig = await pullConfig({ configName }).catch(() => PRODUCTION && process.exit(1));//exit program if pulling config failed
        config.set(_.merge(defaultConfig, process.env, cloudConfig));
    } else config.set(_.merge(defaultConfig, process.env))

    return "ok"
}

//pull config from aws dynamodb
async function pullConfig({ configName }) {
    try {
        //connect moogoose
        const db = await mongoose.connect(process.env.MONGO_CONFIG_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });

        logger.info(`Pulling config from database...`)
        const ConfigModel = db.model("config", mongoose.Schema({
            name: {
                type: String,
                required: true
            },
            value: {
                type: mongoose.SchemaTypes.Mixed,
                required: true
            }
        }, { collection: 'config' }));

        const config = await ConfigModel.findOne({ name: configName })
        logger.info(`Complete pulling config.`)
        await db.disconnect()

        return config && config.value || {}
    } catch (error) {
        logger.error(`Failed to pull config from database.`, { Reason: error })
        throw `Failed to pull config from database.`
    }
}

module.exports = {
    config,
    loadConf
}
