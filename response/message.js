const sprintf = require('sprintf-js').sprintf
const constants = require('../constants')

const messages = {
    //default error message
    [constants.ERROR_MESSAGE_KEY_NOT_EXIST]: `Error message with key "%1$s" is not exist!`,
    [constants.MISSING_AUTHORIZATION]: `Missing authorization token in header/cookie!`,
    [constants.AUTHORIZATION_FAILED]: `Can't authorize with this token!`,
    [constants.WRONG_AUTHORIZATION_TOKEN]: `Wrong authorization token!`,
    [constants.VALIDATION_ERROR]: `Request input is invalid!`,
    [constants.MISSING_ENVIRONMENT_CONFIG]: `Missing evironment config "%1$s" !`,
    [constants.FORBIDDEN]: `Forbidden`,
    [constants.UNEXPECTED_ERROR]: `An unexpected error happened!`,
    [constants.TOKEN_EXPIRED]: `Token expired!`,
}

/**
 * Load more error message to object "messages"
 * @param {*} msgs 
 */
function load(msgs) {
    return Object.assign(messages, msgs)
}

/**
 * Get message formatted with params by key
 * @param {*} key 
 * @param {*} params 
 * @returns 
 */
function get(key, params = []) {
    //throw error if key is not yet defined else return formatted message
    if (!messages[key]) throw sprintf(messages[constants.ERROR_MESSAGE_KEY_NOT_EXIST], key)
    else return sprintf(messages[key], ...params)
}

module.exports = {
    load,
    get
}