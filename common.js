const { pathToRegexp } = require("path-to-regexp");

function convertToSlug(text) {
    return text
        .toLowerCase()
        .replace(/ /g, '-')
        .replace(/[^\w-]+/g, '')
        ;
}

function randStr(length) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

function matchPath(pathRegex, path) {
    const regexp = pathToRegexp(pathRegex);
    const ok = regexp.exec(path);
    if (!ok) return false;
    return true;
}

function replaceTextSearch(text, caseSensitive = true, opt = "pg") {
    let resultStr = caseSensitive ? text : text.toLowerCase();
    switch (opt) {
        case "pg": {
            resultStr = resultStr.replace(new RegExp(/\\/, "g"), "\\\\");
            resultStr = resultStr.replace(new RegExp("_", "g"), "\\_");
            resultStr = resultStr.replace(new RegExp("%", "g"), "\\%");
            break;
        }
        default: {
            break;
        }
    }

    return resultStr;
}


module.exports = {
    convertToSlug,
    randStr,
    matchPath,
    replaceTextSearch
}