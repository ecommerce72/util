// reference from https://github.com/Abazhenov/express-async-handler

const asyncUtil = fn => function asyncUtilWrap(req, res, next) {
  const fnReturn = fn(req, res, next)
  return Promise.resolve(fnReturn)
    .then(result => {
      if (result !== undefined) return res.send(result)
    })
    .catch(next)
}

module.exports = asyncUtil