function validateSchema(joiSchema, object) {
    // if (joi.isSchema(joiSchema)) throw `Schema of validation isn't valid.`

    let errors = []
    const result = joiSchema.error(errs => {
        errs.map(err => {
            switch (err.code) {
                case "number.base":
                    errors.push(`${err.local.label} must be a number.`);
                    break;
                case "string.base":
                    errors.push(`${err.local.label} must be a string.`);
                    break;
                case "any.required":
                    errors.push(`${err.local.label} is required.`);
                    break;
                case "array.base":
                    errors.push(`${err.local.label} must be an array.`);
                    break;
                case "boolean.base":
                    errors.push(`${err.local.label} must be a boolean.`);
                    break;
                case "date.base":
                    errors.push(`${err.local.label} must be a date.`);
                    break;
                case "date.format":
                    errors.push(`${err.local.label} doesn't match the required format.`);
                    break;
                case "date.strict":
                    errors.push(`${err.local.label} must be a date.`);
                    break;
                case "string.email":
                    errors.push(`${err.local.label} must be an email.`);
                    break;
                case "string.empty":
                    errors.push(`${err.local.label} must not be empty.`);
                    break;
                case "string.isoDate":
                    errors.push(`${err.local.label} must be an isoDate.`);
                    break;
                case "number.integer":
                    errors.push(`${err.local.label} must be an integer.`);
                    break;
                case "array.includesRequiredUnknowns":
                    errors.push(`${err.local.label} must include at least one item.`);
                    break;
                case "any.only":
                    errors.push(`${err.local.label} must be one of the following values ${err.local.valids}.`);
                    break;
                case "object.unknown":
                    errors.push(`Parameter ${err.local.label} isn't supported.`);
                    break
                case "string.pattern.base": {
                    errors.push(`${err.local.label} must match the required pattern ${err.local.regex}.`);
                    break
                }
                default:
                    throw new Error(`Non-override error message for joi code: ${err.code}.\n${err}`)
            }
        });
        return errs
    }).validate(object, { abortEarly: false })
    return {
        errors,
        value: result.value
    }
}
module.exports = {
    validateSchema
}